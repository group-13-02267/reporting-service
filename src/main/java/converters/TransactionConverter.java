package converters;

import DTO.PaymentTransaction;
import entities.CustomerTransaction;
import entities.MerchantTransaction;

/**
 * Defines methods for converting a {@link PaymentTransaction} to other types of transactions
 * @author Mathias
 */
public interface TransactionConverter {
    CustomerTransaction toCustomerTransaction(PaymentTransaction paymentTransaction, boolean isRefund);
    MerchantTransaction toMerchantTransaction(PaymentTransaction paymentTransaction, boolean isRefund);
}
