package servicetests;

import DTO.ReportDTO;
import DTO.ReportGenerationRequestDTO;
import converters.DTUTransactionConverter;
import messaging.DTUReportingMessagingService;
import messaging.MessageSender;
import messaging.RabbitMqMessageService;
import repositories.customer.CustomerTransactionRepository;
import repositories.customer.InMemoryCustomerTransactionRepository;
import repositories.mercant.InMemoryMerchantTransactionRepository;
import repositories.mercant.MerchantTransactionRepository;
import services.*;

import static org.junit.Assert.assertEquals;

/**
 * Defines common functionality and fields used by {@link CustomerReportGenerationSteps} and {@link MerchantReportGenerationSteps}
 * @author Mathias
 */
public abstract class ReportGenerationStepBase {
    protected ReportDTO result;
    protected String actualRoute;
    protected MessageSender messageSender;
    protected ReportGenerationRequestDTO reportGenerationRequest;
    protected String user;
    protected String reportRequestId;
    protected GenerateReportService customerGenerateReportService;
    protected GenerateReportService merchantGenerateReportService;


    public ReportGenerationStepBase(){
        messageSender = new RabbitMqMessageService<ReportDTO>() {
            @Override
            public void sendMessage(ReportDTO content, String route, String exchange) {
                result = content;
                assertEquals("reporting", exchange);
                actualRoute = route;
            }
        };
    }

    protected void baseSetup(){
        MerchantTransactionRepository merchantTransactionRepository = InMemoryMerchantTransactionRepository.getInstance();
        CustomerTransactionRepository customerTransactionRepository = InMemoryCustomerTransactionRepository.getInstance();

        ReportingService reportingService = new DTUReportingService(customerTransactionRepository, merchantTransactionRepository, new DTUTransactionConverter());
        DTUReportingMessagingService reportingMessagingService = new DTUReportingMessagingService(messageSender);
        customerGenerateReportService = new CustomerGenerateReportService(reportingService, reportingMessagingService);
        merchantGenerateReportService = new MerchantGenerateReportService(reportingService, reportingMessagingService);
    }
}
