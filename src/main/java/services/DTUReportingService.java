package services;

import DTO.CustomerTransactionDTO;
import DTO.MerchantTransactionDTO;
import DTO.PaymentTransactionDTO;
import converters.TransactionConverter;
import DTO.PaymentTransaction;
import com.google.gson.Gson;
import entities.CustomerTransaction;
import entities.MerchantTransaction;
import repositories.customer.CustomerTransactionRepository;
import repositories.mercant.MerchantTransactionRepository;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service responsiple for delegating the generation reports and reporting an incomming transaction
 * @author Mathias
 */
public class DTUReportingService implements ReportingService {

    private final CustomerTransactionRepository customerTransactionRepository;
    private final MerchantTransactionRepository merchantTransactionRepository;
    private final TransactionConverter transactionConverter;

    public DTUReportingService(CustomerTransactionRepository customerTransactionRepository,
                               MerchantTransactionRepository merchantTransactionRepository,
                               TransactionConverter transactionConverter) {
        this.customerTransactionRepository = customerTransactionRepository;
        this.merchantTransactionRepository = merchantTransactionRepository;
        this.transactionConverter = transactionConverter;
    }

    @Override
    public void reportTransaction(PaymentTransactionDTO paymentTransaction, boolean isRefund) {
        if (paymentTransaction.status){
            CustomerTransaction customerTransaction = transactionConverter.toCustomerTransaction(paymentTransaction, isRefund);
            customerTransactionRepository.add(customerTransaction);

            MerchantTransaction merchantTransaction = transactionConverter.toMerchantTransaction(paymentTransaction, isRefund);
            merchantTransactionRepository.add(merchantTransaction);
        }
    }

    @Override
    public String generateCustomerReport(String customerId, LocalDate startDate, LocalDate endDate) {
        List<CustomerTransaction> customerTransactions = customerTransactionRepository.getCustomerTransactions(customerId, startDate, endDate);

        List<CustomerTransactionDTO> customerTransactionsDTOs = customerTransactions.stream().map(t ->
                new CustomerTransactionDTO(t.getCustomerId(), t.getToken(), t.getAmount(), t.getDate())).collect(Collectors.toList());

        return new Gson().toJson(customerTransactionsDTOs);
    }

    @Override
    public String generateMerchantReport(String merchantId, LocalDate startDate, LocalDate endDate) {
        List<MerchantTransaction> merchantTransactions = merchantTransactionRepository.getMerchantTransactions(merchantId, startDate, endDate);

        List<MerchantTransactionDTO> merchantTransactionDTOs = merchantTransactions.stream().map(t ->
                new MerchantTransactionDTO(t.getMerchantId(), t.getToken(), t.getAmount(), t.getDate())).collect(Collectors.toList());

        return new Gson().toJson(merchantTransactionDTOs);
    }
}
