package entities;

/**
 * Represents the methods a merchant transaction should have
 * @author Mathias
 */
public interface MerchantTransaction extends BaseTransaction {
    String getMerchantId();
}
