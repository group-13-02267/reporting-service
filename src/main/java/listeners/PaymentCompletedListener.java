package listeners;

import DTO.PaymentTransactionDTO;
import com.google.gson.Gson;
import core.AsyncService;
import providers.ExchangeProvider;
import services.ReportingService;

/**
 * Listener for listening on completed payments
 * @author Mathias
 */
public class PaymentCompletedListener extends AsyncService<PaymentTransactionDTO> {

    private final ReportingService reportingService;

    public PaymentCompletedListener(ReportingService reportingService) {
        super(ExchangeProvider.getPaymentExchange(), "transaction.payment.handled.*");
        this.reportingService = reportingService;
    }

    @Override
    protected PaymentTransactionDTO handleMessage(String message) {
        System.out.println("I got this:" + message);
        return new Gson().fromJson(message, PaymentTransactionDTO.class);
    }

    @Override
    public void receive(PaymentTransactionDTO paymentResponseDTO) {
        reportingService.reportTransaction(paymentResponseDTO, false);
    }
}
