package converters;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;

import static java.time.temporal.ChronoField.*;

/**
 *  Contains methods for converting a {@link String} to a {@link LocalDate}, and from {@link LocalDate} to {@link String}
 * @author David
 */
public class DatetimeConverter {
    private static DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('-')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('-')
            .appendValue(YEAR, 4)
            .toFormatter();

    /**
     * @param date in the format dd-mm-yyyy
     * @return {@link LocalDate} if correct format else null
     */
    public static LocalDate ConvertStringToDate(String date) {
        try {
            return LocalDate.parse(date, formatter);
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    /**
     * Converts a {@link LocalDate} to a {@link String}
     * @param date an instance of {@link LocalDate}
     * @return a {@link String} in the format dd-mm-yyyy
     */
    public static String convertDateToString(LocalDate date) {
        return date.format(formatter);
    }

}
