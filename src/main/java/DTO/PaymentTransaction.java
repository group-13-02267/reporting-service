package DTO;

import java.time.LocalDate;

/**
 * Represents the set of getters that should be present of a payment transaction
 * @author Mathias
 */
public interface PaymentTransaction {
    String getSender();
    String getReceiver();
    float getAmount();
    String getToken();
    LocalDate getDate();
}
