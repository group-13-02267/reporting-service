package messaging;

/**
 * Interface for broadcasting a message
 * @author Mathias
 */
public interface MessageSender<T> {
    void sendMessage(T content, String route, String exchange);
}
