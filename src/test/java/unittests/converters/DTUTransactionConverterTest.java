package unittests.converters;

import DTO.PaymentTransactionDTO;
import converters.DTUTransactionConverter;
import converters.TransactionConverter;
import entities.CustomerTransaction;
import entities.MerchantTransaction;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unittest of {@link DTUTransactionConverter}
 * @author Mathias
 */
public class DTUTransactionConverterTest {

    private TransactionConverter transactionConverter;
    private PaymentTransactionDTO testPaymentDTO = new PaymentTransactionDTO(true, "sender", "token", "receiver",
                                                                            100, "16-08-2016");

    @Before
    public void setup(){
        transactionConverter = new DTUTransactionConverter();
    }

    @Test
    public void toCustomerTransaction_whenNotrefund_ExpectConvertedToCorrectCustomerTransaction(){
        CustomerTransaction customerTransaction = transactionConverter.toCustomerTransaction(testPaymentDTO, false);
        assertEquals(testPaymentDTO.sender, customerTransaction.getCustomerId());
        assertTrue(customerTransaction.getAmount() == testPaymentDTO.amount);
        assertEquals(testPaymentDTO.token, customerTransaction.getToken());
        assertNotNull(customerTransaction.getDate());
    }

    @Test
    public void toCustomerTransaction_whenRefund_ExpectCustomerIsReceiver(){
        CustomerTransaction customerTransaction = transactionConverter.toCustomerTransaction(testPaymentDTO, true);
        assertEquals(testPaymentDTO.getReceiver(), customerTransaction.getCustomerId());
    }

    @Test
    public void toMerchantTransaction_whenRefund_ExpectMerchantIsSender(){
        MerchantTransaction merchantTransaction = transactionConverter.toMerchantTransaction(testPaymentDTO, true);
        assertEquals(testPaymentDTO.getSender(), merchantTransaction.getMerchantId());
    }

    @Test
    public void toMerchantTransaction_whenNotrefund_ExpectConvertedToCorrectMerchantTransaction(){
        MerchantTransaction merchantTransaction = transactionConverter.toMerchantTransaction(testPaymentDTO, false);
        assertEquals(testPaymentDTO.receiver, merchantTransaction.getMerchantId());
        assertTrue(merchantTransaction.getAmount() == testPaymentDTO.amount);
        assertEquals(testPaymentDTO.token, merchantTransaction.getToken());
        assertNotNull(merchantTransaction.getDate());
    }
}
