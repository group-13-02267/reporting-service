package servicetests;

import DTO.CustomerTransactionDTO;
import DTO.ReportGenerationRequestDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import converters.DatetimeConverter;
import entities.CustomerTransaction;
import entities.DTUCustomerTransaction;
import helpers.EqualityAsserterHelper;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import listeners.GenerateCustomerReportListener;
import repositories.customer.InMemoryCustomerTransactionRepository;
import java.lang.reflect.Type;
import java.util.ArrayList;
import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

/**
 * Steps concerning the creation of customer reports
 * @author Mathias
 */
public class CustomerReportGenerationSteps extends ReportGenerationStepBase{

    CustomerTransaction testTransaction;

    public CustomerReportGenerationSteps(){
        super();
    }

    @When("there exists transactions for a customer")
    public void thereExistsTransactionsForACustomer() {
        user = "customer";
        reportRequestId = "anotherTestId";
        InMemoryCustomerTransactionRepository customerTransactionRepository = InMemoryCustomerTransactionRepository.getInstance();
        testTransaction = new DTUCustomerTransaction("test", 100, user, DatetimeConverter.ConvertStringToDate("18-01-2020"));
        customerTransactionRepository.add(testTransaction);
    }

    @When("the service is asked to generate a report for that customer")
    public void theServiceIsAskedToGenerateAReportForThatUser() {
        baseSetup();

        GenerateCustomerReportListener reportListener = new GenerateCustomerReportListener(customerGenerateReportService);
        reportGenerationRequest = new ReportGenerationRequestDTO(reportRequestId, user, "15-01-2020", "20-01-2020");
        reportListener.receive(reportGenerationRequest);
    }

    @Then("it generates the customer report correctly")
    public void itGeneratesTheCustomerReportCorrectly() {
        assertNotNull(result);
        assertTrue(result.success);
        Type listType = new TypeToken<ArrayList<CustomerTransactionDTO>>(){}.getType();
        ArrayList<CustomerTransactionDTO> transactionDTOS = new Gson().fromJson(result.report, listType);

        assertNotNull(transactionDTOS);
        assertEquals(1, transactionDTOS.size());
        CustomerTransactionDTO actuanMerchanTransactionDTO = transactionDTOS.get(0);
        EqualityAsserterHelper.assertCustomerTransactionAreEqual(testTransaction, actuanMerchanTransactionDTO);
    }

    @Then("the customer report is broadcastet")
    public void theCustomerReportIsBroadcastet() {
        assertEquals(actualRoute, "report.generated." + reportRequestId);
    }


}
