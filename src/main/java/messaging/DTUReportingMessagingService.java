package messaging;

import DTO.ReportDTO;
import providers.ExchangeProvider;

/**
 * Helper method for broadcasting a report
 * @author Mathias
 */
public class DTUReportingMessagingService implements ReportingMessagingService {

    private final MessageSender messageSender;

    public DTUReportingMessagingService(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @Override
    public void SendReport(String report, String reportId) {
        messageSender.sendMessage(new ReportDTO(report != null, report),
                           "report.generated." + reportId, ExchangeProvider.getReportingExchange());

    }
}
