package services;

import DTO.PaymentTransactionDTO;

import java.time.LocalDate;

/**
 * Defines the functionality of the reporting service
 * @author Mathias
 */
public interface ReportingService {
    void reportTransaction(PaymentTransactionDTO paymentTransaction, boolean isRefund);
    String generateCustomerReport(String customerId, LocalDate startDate, LocalDate endDate);
    String generateMerchantReport(String merchantId, LocalDate startDate, LocalDate endDate);
}
