package entities;

import java.time.LocalDate;

/**
 * Interface over all the methods all transaction entities in the system should have
 * @author Mathias
 */
public interface BaseTransaction {
    String getToken();
    float getAmount();
    LocalDate getDate();
}
