package DTO;

import converters.DatetimeConverter;

import java.time.LocalDate;

/**
 * Class for representing the base things that merchant and customer transactions have in common
 * @author Mathias
 */
public abstract class BaseTransactionDTO {
    public String token;
    public float amount;
    public String date;

    public BaseTransactionDTO(String token, float amount, LocalDate date) {
        this.token = token;
        this.amount = amount;
        this.date = DatetimeConverter.convertDateToString(date);;
    }
}
