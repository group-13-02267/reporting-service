package entities;

import java.time.LocalDate;

/**
 * Implementation of a {@link CustomerTransaction}
 * @author Mathias
 */
public class DTUCustomerTransaction extends DTUBaseTransaction implements CustomerTransaction {
    private String customerId;

    public DTUCustomerTransaction(String token, float amount, String customerId, LocalDate creationTime) {
        super(token, amount, creationTime);
        this.customerId = customerId;
    }

    @Override
    public String getCustomerId() {
        return customerId;
    }
}
