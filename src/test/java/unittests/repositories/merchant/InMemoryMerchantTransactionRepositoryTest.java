package unittests.repositories.merchant;

import entities.CustomerTransaction;
import entities.DTUCustomerTransaction;
import entities.DTUMerchantTransaction;
import entities.MerchantTransaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import repositories.customer.InMemoryCustomerTransactionRepository;
import repositories.mercant.InMemoryMerchantTransactionRepository;
import repositories.mercant.MerchantTransactionRepository;
import unittests.repositories.RepositoryTestBase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unittest of {@link InMemoryMerchantTransactionRepository}
 * @author Mathias
 */
public class InMemoryMerchantTransactionRepositoryTest extends RepositoryTestBase {

    private MerchantTransactionRepository merchantTransactionRepository;
    private final String merchantId = "merchantId";
    @Before
    public void setup(){
        merchantTransactionRepository = InMemoryMerchantTransactionRepository.getInstance();
    }

    @Test
    public void getMercantTransactions_whenDatabaseIsEmpty_expectEmptyResult(){
        List<MerchantTransaction> transactions = merchantTransactionRepository.getMerchantTransactions(merchantId,
                LocalDate.now().minusDays(5), LocalDate.now().plusDays(5));
        Assert.assertEquals(0, transactions.size());
    }

    @Test
    public void addGet_whenToAndFromIsTheSameDate_expectAllReportsForThatDay() {
        LocalDate baseDate =  LocalDate.now();

        List<MerchantTransaction> merchantTransactions = new ArrayList<>();
        MerchantTransaction expected = new DTUMerchantTransaction("hello", 11.1f, merchantId, baseDate);
        merchantTransactions.add(expected);
        merchantTransactions.add(new DTUMerchantTransaction("test", 232, merchantId, baseDate.plusDays(1)));
        merchantTransactions.add(new DTUMerchantTransaction("hest", 5421, merchantId, baseDate.minusDays(1)));

        addMerchantTransactionsHelper(merchantTransactions);

        List<MerchantTransaction> actual = merchantTransactionRepository.getMerchantTransactions(merchantId, baseDate, baseDate);
        assertEquals(1, actual.size());
        assertEquals(merchantTransactions.get(0), actual.get(0)); // size is one if we reach this point so we just compare
    }


    @Test
    public void addGet_whenNonEmptyReportsForMerchant_expectAllInGivenRange() {
        DTUMerchantTransaction testTransaction = new DTUMerchantTransaction("token", 100, merchantId, LocalDate.now());
        DTUMerchantTransaction anotherTestTransaction = new DTUMerchantTransaction("token", 100, merchantId, LocalDate.now().minusDays(2));
        DTUMerchantTransaction thirdTestTransaction = new DTUMerchantTransaction("token", 100, merchantId, LocalDate.now().minusDays(10));

        merchantTransactionRepository.add(testTransaction);
        merchantTransactionRepository.add(anotherTestTransaction);
        merchantTransactionRepository.add(thirdTestTransaction);

        List<MerchantTransaction> actual = merchantTransactionRepository.getMerchantTransactions(merchantId, LocalDate.now().minusDays(5), LocalDate.now().plusDays(3));
        assertEquals(2, actual.size());
        assertEquals(actual.get(0), testTransaction);
        assertEquals(actual.get(1), anotherTestTransaction);
    }

    private void addMerchantTransactionsHelper(List<MerchantTransaction> merchantTransactions){
        for (MerchantTransaction transaction : merchantTransactions){
            merchantTransactionRepository.add(transaction);
        }
    }
}
