package unittests.services;

import DTO.CustomerTransactionDTO;
import DTO.MerchantTransactionDTO;
import DTO.PaymentTransactionDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import converters.TransactionConverter;
import entities.CustomerTransaction;
import entities.DTUCustomerTransaction;
import entities.DTUMerchantTransaction;
import entities.MerchantTransaction;
import helpers.EqualityAsserterHelper;
import org.junit.Before;
import org.junit.Test;
import repositories.customer.CustomerTransactionRepository;
import repositories.mercant.MerchantTransactionRepository;
import services.DTUReportingService;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * unittest of {@link DTUReportingServiceTest}
 * @author Mathias
 */
public class DTUReportingServiceTest {
    private CustomerTransactionRepository customerTransactionRepository;
    private MerchantTransactionRepository merchantTransactionRepository;
    private TransactionConverter transactionConverter;
    private DTUReportingService reportingService;
    private String customerId = "customerId";
    private String merchantId = "merchantId";
    private LocalDate date;

    @Before
    public void setup(){
        customerTransactionRepository = mock(CustomerTransactionRepository.class);
        merchantTransactionRepository = mock(MerchantTransactionRepository.class);
        transactionConverter = mock(TransactionConverter.class);
        reportingService = new DTUReportingService(customerTransactionRepository, merchantTransactionRepository, transactionConverter);
        date = LocalDate.now();
    }

    @Test
    public void generateCustomerReport_whenInvoked_expectCorrectResponse(){
        //setup
        LocalDate start = LocalDate.now().minusDays(1);
        LocalDate end = LocalDate.now().plusDays(1);
        String token = "testToken";
        float amount = 100;
        DTUCustomerTransaction transaction = new DTUCustomerTransaction(token, amount, customerId, date);
        List<CustomerTransaction> transactions = Arrays.asList(transaction);
        when(customerTransactionRepository.getCustomerTransactions(customerId, start, end)).thenReturn(transactions);
        //act
        String actual = reportingService.generateCustomerReport(customerId, start, end);
        //assert
        verify(customerTransactionRepository).getCustomerTransactions(customerId, start, end);
        assertNotNull(actual);
        Type listType = new TypeToken<ArrayList<CustomerTransactionDTO>>(){}.getType();
        ArrayList<CustomerTransactionDTO> actualAsTransactionList = new Gson().fromJson(actual, listType);
        CustomerTransactionDTO actualTransaction = actualAsTransactionList.get(0);

        EqualityAsserterHelper.assertCustomerTransactionAreEqual(transaction, actualTransaction);
    }

    @Test
    public void reportTransaction_whenInvoked_expectConvertersAndRepositoryAreCalledWithCorrectParameters(){
        //setup
        boolean isRefund = true;
        PaymentTransactionDTO paymentTransactionDTO = new PaymentTransactionDTO();
        paymentTransactionDTO.status = true;
        DTUMerchantTransaction dtuMerchantTransaction = new DTUMerchantTransaction("onetoken", 1, merchantId, date);
        DTUCustomerTransaction dtuCustomerTransaction = new DTUCustomerTransaction("anotherToken", 33, merchantId, date);
        when(transactionConverter.toCustomerTransaction(paymentTransactionDTO, isRefund)).thenReturn(dtuCustomerTransaction);
        when(transactionConverter.toMerchantTransaction(paymentTransactionDTO, isRefund)).thenReturn(dtuMerchantTransaction);
        //act
        reportingService.reportTransaction(paymentTransactionDTO, isRefund);
        //assert
        verify(customerTransactionRepository).add(dtuCustomerTransaction);
        verify(merchantTransactionRepository).add(dtuMerchantTransaction);
    }

    @Test
    public void generateMerchantReport_whenInvoked_expectCorrectResponse(){
        //setup
        LocalDate start = LocalDate.now().minusDays(1);
        LocalDate end = LocalDate.now().plusDays(1);
        String token = "anotherTestToken";
        float amount = 1.1f;
        DTUMerchantTransaction transaction = new DTUMerchantTransaction(token, amount, merchantId, date);
        List<MerchantTransaction> transactions = Arrays.asList(transaction);
        when(merchantTransactionRepository.getMerchantTransactions(merchantId, start, end)).thenReturn(transactions);
        //act
        String actual = reportingService.generateMerchantReport(merchantId, start, end);
        //assert
        verify(merchantTransactionRepository).getMerchantTransactions(merchantId, start, end);
        assertNotNull(actual);
        Type listType = new TypeToken<ArrayList<MerchantTransactionDTO>>(){}.getType();
        ArrayList<MerchantTransactionDTO> actualAsTransactionList = new Gson().fromJson(actual, listType);
        MerchantTransactionDTO actualTransaction = actualAsTransactionList.get(0);
        EqualityAsserterHelper.assertMerchantTransactionsAreEqual(transaction, actualTransaction);
    }
}


