package unittests.repositories;

import org.junit.After;
import repositories.customer.InMemoryCustomerTransactionRepository;
import repositories.mercant.InMemoryMerchantTransactionRepository;

/**
 * Step for cleaning up the in-memory databases after each repository test
 * @author Mathias
 */
public class RepositoryTestBase {
    @After
    public void cleanUp(){
        InMemoryMerchantTransactionRepository.clean();
        InMemoryCustomerTransactionRepository.clean();
    }
}
