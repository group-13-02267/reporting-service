Feature: Report generation feature

  Scenario: Successful merchant report
    When there exists transactions for a merchant
    And  the service is asked to generate a report for that merchant
    Then it generates the merchant report correctly
    And  the merchant report is broadcastet

  Scenario: Successful customer report
    When there exists transactions for a customer
    And  the service is asked to generate a report for that customer
    Then it generates the customer report correctly
    And  the customer report is broadcastet
