package DTO;

import java.time.LocalDate;

/**
 * Represents a customer transaction which is the information stored in a customer report
 * @author Mathias
 */
public class CustomerTransactionDTO extends BaseTransactionDTO {

    public String customerId;

    public CustomerTransactionDTO(String customerId, String token, float amount, LocalDate date) {
        super(token, amount, date);
        this.customerId = customerId;
    }
}
