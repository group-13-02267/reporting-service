package repositories.mercant;

import entities.MerchantTransaction;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * In-memory repository implementation of {@link MerchantTransactionRepository}
 * @author Mathias
 */
public class InMemoryMerchantTransactionRepository implements MerchantTransactionRepository{
    private static InMemoryMerchantTransactionRepository instance;

    public static InMemoryMerchantTransactionRepository getInstance() {
        if (instance == null) {
            instance = new InMemoryMerchantTransactionRepository();
        }
        return instance;
    }

    List<MerchantTransaction> merchantTransactions = new ArrayList<>();

    /***
     * Helper method for cleaning all data since the repository is a singleton
     */
    public static void clean() {
        InMemoryMerchantTransactionRepository.getInstance().merchantTransactions = new ArrayList<>();
    }

    protected InMemoryMerchantTransactionRepository() {
    }

    @Override
    public MerchantTransaction add(MerchantTransaction transaction) {
        merchantTransactions.add(transaction);
        return transaction;
    }

    @Override
    public List<MerchantTransaction> getMerchantTransactions(String merchantId, LocalDate from, LocalDate to) {
        return merchantTransactions.stream()
                .filter((p) -> p.getMerchantId().equals(merchantId) && p.getDate().isBefore(to.plusDays(1)) &&
                         p.getDate().isAfter(from.minusDays(1))).collect(Collectors.toList());
    }
}
