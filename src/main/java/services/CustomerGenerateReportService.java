package services;

import messaging.ReportingMessagingService;

import java.time.LocalDate;

/**
 * Service responsiple for generating reports for a customer
 * @author Mathias
 */
public class CustomerGenerateReportService implements GenerateReportService {
    private final ReportingService reportingService;
    private final ReportingMessagingService reportingMessagingService;

    public CustomerGenerateReportService(ReportingService reportingService, ReportingMessagingService reportingMessagingService) {
        this.reportingService = reportingService;
        this.reportingMessagingService = reportingMessagingService;
    }

    @Override
    public void requestReport(String userId, LocalDate startDate, LocalDate endDate, String requestId) {
        String report = reportingService.generateCustomerReport(userId, startDate, endDate);
        reportingMessagingService.SendReport(report, requestId);
    }
}
