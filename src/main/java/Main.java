import DTO.PaymentTransactionDTO;
import converters.DTUTransactionConverter;
import core.AsyncException;
import core.AsyncListener;
import core.Microservice;
import listeners.GenerateCustomerReportListener;
import listeners.GenerateMerchantReportListener;
import listeners.PaymentCompletedListener;
import listeners.RefundCompletedListener;
import messaging.DTUReportingMessagingService;
import messaging.RabbitMqMessageService;
import repositories.customer.InMemoryCustomerTransactionRepository;
import repositories.mercant.InMemoryMerchantTransactionRepository;
import services.*;


/**
 * Starts the service and sets up the different listeners of the service
 * @author Mathias
 */
public class Main extends Microservice {

    public static void main(String[] args) throws Exception {
        Microservice service = new Main();
        System.out.println("Starting reporting Service...");
        service.startup();
    }

    @Override
    public void startup() throws AsyncException {
        ReportingService reportingService = new DTUReportingService(InMemoryCustomerTransactionRepository.getInstance(),
                InMemoryMerchantTransactionRepository.getInstance(), new DTUTransactionConverter());
        DTUReportingMessagingService dtuReportingMessagingService = new DTUReportingMessagingService(new RabbitMqMessageService());

        // setup listener for listening on customer report requests
        GenerateReportService customerGenerateReportService = new CustomerGenerateReportService(reportingService, dtuReportingMessagingService);
        GenerateCustomerReportListener generateCustomerReportListener = new GenerateCustomerReportListener(customerGenerateReportService);
        generateCustomerReportListener.listen();

        // setup listener for listening on merchant report requests
        GenerateReportService merchantGenerateReportService = new MerchantGenerateReportService(reportingService, dtuReportingMessagingService);
        GenerateMerchantReportListener generateMerchantReportListener = new GenerateMerchantReportListener(merchantGenerateReportService);
        generateMerchantReportListener.listen();

        // setup listener for listening on when a payment is completed
        AsyncListener<PaymentTransactionDTO> paymentCompletedListener =  new PaymentCompletedListener(reportingService);
        paymentCompletedListener.listen();

        // setup listener for listening on when a customer is completed
        AsyncListener<PaymentTransactionDTO> refundCompletedListener =  new RefundCompletedListener(reportingService);
        refundCompletedListener.listen();
    }
}