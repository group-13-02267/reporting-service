package DTO;

/**
 * Represents a generated report
 * @author Mathias
 */
public class ReportDTO {

    public ReportDTO(boolean success, String report) {
        this.success = success;
        this.report = report;
    }

    public ReportDTO() {
    }

    public boolean success;
    public String report;
}
