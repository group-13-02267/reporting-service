package messaging;

import core.AsyncSender;
import core.RabbitSender;

/**
 * Class for sending message via RabbitMq
 * @author Mathias
 */
public class RabbitMqMessageService<T> implements MessageSender<T> {
    @Override
    public void sendMessage(T content, String route, String exchange) {
        AsyncSender eventSender = new RabbitSender(exchange, route);
        eventSender.send(content);
    }
}