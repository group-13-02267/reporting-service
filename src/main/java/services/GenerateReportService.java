package services;

import java.time.LocalDate;

/**
 * Defines the methods for requesting a report
 */
public interface GenerateReportService {
    void requestReport(String userId, LocalDate startDate, LocalDate endDate, String requestId);
}
