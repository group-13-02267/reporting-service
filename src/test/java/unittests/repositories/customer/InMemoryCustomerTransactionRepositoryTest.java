package unittests.repositories.customer;

import entities.CustomerTransaction;
import entities.DTUCustomerTransaction;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import repositories.customer.CustomerTransactionRepository;
import repositories.customer.InMemoryCustomerTransactionRepository;
import unittests.repositories.RepositoryTestBase;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unittest of {@link InMemoryCustomerTransactionRepository}
 * @author Mathias
 */
public class InMemoryCustomerTransactionRepositoryTest extends RepositoryTestBase {

    private CustomerTransactionRepository customerTransactionRepository;
    private final String customerId = "customerId";
    @Before
    public void setup(){
        customerTransactionRepository = InMemoryCustomerTransactionRepository.getInstance();
    }

    @Test
    public void getCustomerTransaciton_whenDatabaseIsEmpty_expectEmptyResult() {
            List<CustomerTransaction> transactions = customerTransactionRepository.getCustomerTransactions(customerId,
                    LocalDate.now().minusDays(5), LocalDate.now().plusDays(5));
            Assert.assertEquals(0, transactions.size());
    }

    @After
    public void cleanUp(){
        InMemoryCustomerTransactionRepository.clean();
    }

    @Test
    public void addGet_whenNonEmptyReportsForMerchant_expectAllInGivenRange() {
        DTUCustomerTransaction testTransaction = new DTUCustomerTransaction("token", 100, customerId, LocalDate.now());
        DTUCustomerTransaction anotherTestTransaction = new DTUCustomerTransaction("token", 232, customerId, LocalDate.now().minusDays(2));
        DTUCustomerTransaction thirdTestTransaction = new DTUCustomerTransaction("token", 1256.232f, customerId, LocalDate.now().minusDays(10));

        customerTransactionRepository.add(testTransaction);
        customerTransactionRepository.add(anotherTestTransaction);
        customerTransactionRepository.add(thirdTestTransaction);

        List<CustomerTransaction> actual = customerTransactionRepository.getCustomerTransactions(customerId, LocalDate.now().minusDays(5), LocalDate.now().plusDays(3));
        assertEquals(2, actual.size());
        assertEquals(actual.get(0), testTransaction);
        assertEquals(actual.get(1), anotherTestTransaction);
    }

    @Test
    public void addGet_whenToAndFromIsTheSameDate_expectAllReportsForThatDay() {
        LocalDate baseDate =  LocalDate.now();

        List<CustomerTransaction> customerTransactions = new ArrayList<>();
        DTUCustomerTransaction expected = new DTUCustomerTransaction("1", 100, customerId, baseDate);
        customerTransactions.add(expected);
        customerTransactions.add(new DTUCustomerTransaction("2", 232, customerId, baseDate.plusDays(1)));
        customerTransactions.add(new DTUCustomerTransaction("3", 1256.232f, customerId, baseDate.minusDays(1)));

        addCustomerTransactionsHelper(customerTransactions);

        List<CustomerTransaction> actual = customerTransactionRepository.getCustomerTransactions(customerId, baseDate, baseDate);
        assertEquals(1, actual.size());
        assertEquals(customerTransactions.get(0), actual.get(0)); // size is one if we reach this point so we just compare
    }

    private void addCustomerTransactionsHelper(List<CustomerTransaction> customerTransactions){
        for (CustomerTransaction transaction : customerTransactions){
            customerTransactionRepository.add(transaction);
        }
    }
}
