package entities;

import java.time.LocalDate;

/**
 * Implementation of a {@link MerchantTransaction}
 * @author Mathias
 */
public class DTUMerchantTransaction extends DTUBaseTransaction implements MerchantTransaction {
    private String merchantId;
    public DTUMerchantTransaction(String token, float amount, String merchantId, LocalDate creationTime) {
        super(token, amount, creationTime);
        this.merchantId = merchantId;
    }

    @Override
    public String getMerchantId() {
        return merchantId;
    }
}
