package entities;

/**
 * Represents the methods a customer transaction should have
 * @author Mathias
 */
public interface CustomerTransaction extends BaseTransaction {
    String getCustomerId();
}
