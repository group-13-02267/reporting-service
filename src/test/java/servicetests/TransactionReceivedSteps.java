package servicetests;

import DTO.PaymentTransactionDTO;
import converters.DTUTransactionConverter;
import converters.DatetimeConverter;
import entities.BaseTransaction;
import entities.CustomerTransaction;
import entities.MerchantTransaction;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import listeners.PaymentCompletedListener;
import listeners.RefundCompletedListener;
import repositories.customer.InMemoryCustomerTransactionRepository;
import repositories.mercant.InMemoryMerchantTransactionRepository;
import services.DTUReportingService;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Steps for testing if the logic for receiving transactions is working as intended
 * @author Mathias
 */
public class TransactionReceivedSteps {
    PaymentCompletedListener paymentCompletedListener;
    RefundCompletedListener refundCompletedListener;
    PaymentTransactionDTO paymentTransactionDTO;
    String sender = "user1";
    String receiver = "user2";
    String customer;
    String merchant;

    public TransactionReceivedSteps(){
        DTUReportingService reportingService = new DTUReportingService(InMemoryCustomerTransactionRepository.getInstance(),
                                                                       InMemoryMerchantTransactionRepository.getInstance(),
                                                                       new DTUTransactionConverter());
        paymentCompletedListener = new PaymentCompletedListener(reportingService);
        refundCompletedListener = new RefundCompletedListener(reportingService);
    }


    @When("the service received a payment transaction")
    public void theServiceReceivedAPaymentTransaction() {
        customer = sender;
        merchant = receiver;
        paymentTransactionDTO = createPaymentTransaction();
        paymentCompletedListener.receive(paymentTransactionDTO);
    }

    @When("the service receives a refund transaction")
    public void theServiceReceivesARefundTransaction() {
        customer = receiver;
        merchant = sender;
        paymentTransactionDTO = createPaymentTransaction();
        refundCompletedListener.receive(paymentTransactionDTO);
    }

    @When("the transaction was a success")
    public void thePaymentTransactionWasASuccess() {
        assertTrue(paymentTransactionDTO.status);
    }


    @Then("it correctly converts and persist the transaction as a merchant transaction")
    public void itCorrectlyConvertsAndPersistsTheTransactionAsAMerchantTransaction() {
        LocalDate actualDate = DatetimeConverter.ConvertStringToDate("16-08-2019");
        List<CustomerTransaction> customerTransactions = InMemoryCustomerTransactionRepository.getInstance()
                .getCustomerTransactions(customer, actualDate.minusDays(1), actualDate.plusDays(1));

        assertEquals(1, customerTransactions.size());
        assertPaymentRequestAndCustomertransaction(paymentTransactionDTO, customerTransactions.get(0));
    }

    @Then("it correctly converts and persist the transaction as a customer transaction")
    public void itCorrectlyConvertsAndPersistsTheTransactionAsACustomerTransaction() {
        LocalDate actualDate = DatetimeConverter.ConvertStringToDate("16-08-2019");
        List<MerchantTransaction> merchantTransaction = InMemoryMerchantTransactionRepository.getInstance()
                .getMerchantTransactions(merchant, actualDate.minusDays(1), actualDate.plusDays(1));

        assertEquals(1, merchantTransaction.size());
        assertPaymentRequestAndMerchanttransaction(paymentTransactionDTO, merchantTransaction.get(0));

    }

    private void assertPaymentRequestAndCustomertransaction(PaymentTransactionDTO paymentTransactionDTO,
                                                              CustomerTransaction customerTransaction){

        assertEquals(customer, customerTransaction.getCustomerId());
        assertPaymentRequestAndBasetransaction(paymentTransactionDTO, customerTransaction);
    }

    private void assertPaymentRequestAndMerchanttransaction(PaymentTransactionDTO paymentTransactionDTO,
                                                            MerchantTransaction merchantTransaction){

        assertEquals(merchant, merchantTransaction.getMerchantId());
        assertPaymentRequestAndBasetransaction(paymentTransactionDTO, merchantTransaction);
    }

    private void assertPaymentRequestAndBasetransaction(PaymentTransactionDTO paymentTransactionDTO, BaseTransaction customerTransaction){
        assertEquals(paymentTransactionDTO.token, customerTransaction.getToken());
        assertEquals(paymentTransactionDTO.createdDate, DatetimeConverter.convertDateToString(customerTransaction.getDate()));
        assertTrue(paymentTransactionDTO.amount == customerTransaction.getAmount());
    }

    private PaymentTransactionDTO createPaymentTransaction(){
        return new PaymentTransactionDTO(true, sender, "someToken", receiver,
                112.3f, "16-08-2019");
    }
}
