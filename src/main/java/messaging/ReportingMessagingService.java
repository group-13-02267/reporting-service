package messaging;

/**
 * Interface for sending a report
 * @author Mathias
 */
public interface ReportingMessagingService {
    void SendReport(String report, String reportId);
}
