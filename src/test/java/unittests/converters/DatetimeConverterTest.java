package unittests.converters;

import converters.DatetimeConverter;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Unittest of {@link DatetimeConverter}
 * @author Mathias
 */
public class DatetimeConverterTest {
    @Test
    public void convertStringToDate_whenGivenDateAsString_expectCorrectDate(){
        String date = "16-08-2016";
        LocalDate localDate = DatetimeConverter.ConvertStringToDate(date);
        Assert.assertEquals(8, localDate.getMonthValue());
        Assert.assertEquals(16, localDate.getDayOfMonth());
        Assert.assertEquals(2016, localDate.getYear());
    }

    @Test
    public void convertDateToString_whenGivenDateAsLocalDate_expectCorrectDateAsString(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy");
        String expected = "16-08-2016";
        LocalDate localDate = LocalDate.parse(expected, formatter);

        String actual = DatetimeConverter.convertDateToString(localDate);

        Assert.assertEquals(expected, actual);
    }

}
