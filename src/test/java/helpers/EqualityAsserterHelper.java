package helpers;

import DTO.CustomerTransactionDTO;
import DTO.MerchantTransactionDTO;
import converters.DatetimeConverter;
import entities.CustomerTransaction;
import entities.MerchantTransaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Helper methods for asserting equality of different objects
 * @author Mathias
 */
public class EqualityAsserterHelper {
    public static void assertMerchantTransactionsAreEqual(MerchantTransaction expected, MerchantTransactionDTO actual){
        assertEquals(expected.getMerchantId(), actual.merchantId);
        assertEquals(DatetimeConverter.convertDateToString(expected.getDate()), actual.date);
        assertEquals(expected.getToken(), actual.token);
        assertTrue(actual.amount == actual.amount);
    }

    public static void assertCustomerTransactionAreEqual(CustomerTransaction expected, CustomerTransactionDTO actual){
        assertEquals(expected.getCustomerId(), actual.customerId);
        assertEquals(DatetimeConverter.convertDateToString(expected.getDate()), actual.date);
        assertEquals(expected.getToken(), actual.token);
        assertTrue(actual.amount == actual.amount);
    }
}
