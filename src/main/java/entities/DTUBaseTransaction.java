package entities;

import java.time.LocalDate;

/**
 * Base implementation of all transaction entities
 * @author Mathias
 */
public abstract class DTUBaseTransaction implements BaseTransaction {

    private String token;
    private float amount;
    private LocalDate creationTime;

    protected DTUBaseTransaction(){}

    public DTUBaseTransaction(String token, float amount, LocalDate creationTime) {
        this.token = token;
        this.amount = amount;
        this.creationTime = creationTime;
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public float getAmount() {
        return amount;
    }

    @Override
    public LocalDate getDate() {
        return creationTime;
    }
}
