package listeners;

import DTO.ReportGenerationRequestDTO;
import com.google.gson.Gson;
import converters.DatetimeConverter;
import providers.ExchangeProvider;
import core.AsyncService;
import services.GenerateReportService;

/**
 * Listener for events that requests a report to be generated for a merchant
 * @author Mathias
 */
public class GenerateMerchantReportListener extends AsyncService<ReportGenerationRequestDTO> {
    private final GenerateReportService merchantGenerateReportService;

    public GenerateMerchantReportListener(GenerateReportService merchantGenerateReportService) {
        super(ExchangeProvider.getReportingExchange(), "reporting.merchant.request.*");
        this.merchantGenerateReportService = merchantGenerateReportService;
    }

    @Override
    protected ReportGenerationRequestDTO handleMessage(String message) {
        return new Gson().fromJson(message, ReportGenerationRequestDTO.class);
    }

    @Override
    public void receive(ReportGenerationRequestDTO request) {
        merchantGenerateReportService.requestReport(request.userId, DatetimeConverter.ConvertStringToDate(request.startDate),
                DatetimeConverter.ConvertStringToDate(request.endDate), request.reportGenerationRequestId);
    }
}
