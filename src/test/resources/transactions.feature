Feature: transactions received feature

  Scenario: Payment transaction received
    When the service received a payment transaction
    And  the transaction was a success
    Then it correctly converts and persist the transaction as a merchant transaction
    And it correctly converts and persist the transaction as a customer transaction

  Scenario: Refund transaction received
    When the service receives a refund transaction
    And  the transaction was a success
    Then it correctly converts and persist the transaction as a merchant transaction
    And it correctly converts and persist the transaction as a customer transaction