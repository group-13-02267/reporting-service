package DTO;

import java.io.Serializable;
import java.time.LocalDate;
import converters.DatetimeConverter;

/**
 * Represents the neccesary information which is transfered when making a payment
 * @author George
 */
public class PaymentTransactionDTO implements PaymentTransaction, Serializable {

    public PaymentTransactionDTO() {
    }

    public PaymentTransactionDTO(boolean status, String sender, String token, String receiver, float amount, String date) {
        this.status = status;
        this.sender = sender;
        this.token = token;
        this.receiver = receiver;
        this.amount = amount;
        this.createdDate = date;
    }

    public boolean status;

    public String sender;

    public String token;

    public String receiver;

    public float amount;

    public String createdDate;

    @Override
    public LocalDate getDate() {
        return DatetimeConverter.ConvertStringToDate(createdDate);
    }

    @Override
    public String getSender() {
        return sender;
    }

    @Override
    public String getReceiver() {
        return receiver;
    }

    @Override
    public float getAmount() {
        return amount;
    }

    @Override
    public String getToken() {
        return token;
    }
}