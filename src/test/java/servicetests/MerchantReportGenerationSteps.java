package servicetests;

import DTO.MerchantTransactionDTO;
import DTO.ReportGenerationRequestDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import converters.DatetimeConverter;
import entities.DTUMerchantTransaction;
import entities.MerchantTransaction;
import helpers.EqualityAsserterHelper;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import listeners.GenerateMerchantReportListener;
import repositories.mercant.InMemoryMerchantTransactionRepository;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Steps concerning the generation of merchant reports
 * @author Mathias
 */
public class MerchantReportGenerationSteps extends ReportGenerationStepBase {

    MerchantTransaction testTransaction;

    public MerchantReportGenerationSteps(){
        super();
    }

    @When("there exists transactions for a merchant")
    public void thereExistsTransactionsForAMerchant() {
        user = "merchant";
        reportRequestId = "someId";
        InMemoryMerchantTransactionRepository merchantTransactionRepository = InMemoryMerchantTransactionRepository.getInstance();
        testTransaction = new DTUMerchantTransaction("test", 100, user, DatetimeConverter.ConvertStringToDate("18-01-2020"));
        merchantTransactionRepository.add(testTransaction);
    }


    @When("the service is asked to generate a report for that merchant")
    public void theServiceIsAskedToGenerateAReportForThatUser() {
        baseSetup();

        GenerateMerchantReportListener reportListener = new GenerateMerchantReportListener(merchantGenerateReportService);
        reportGenerationRequest = new ReportGenerationRequestDTO(reportRequestId, user, "15-01-2020", "20-01-2020");
        reportListener.receive(reportGenerationRequest);
    }

    @Then("it generates the merchant report correctly")
    public void itGeneratesTheMerchantReportCorrectly() {
        assertNotNull(result);
        assertTrue(result.success);
        Type listType = new TypeToken<ArrayList<MerchantTransactionDTO>>(){}.getType();
        ArrayList<MerchantTransactionDTO> transactionDTOS = new Gson().fromJson(result.report, listType);

        assertNotNull(transactionDTOS);
        assertEquals(1, transactionDTOS.size());
        MerchantTransactionDTO actuanMerchanTransactionDTO = transactionDTOS.get(0);
        EqualityAsserterHelper.assertMerchantTransactionsAreEqual(testTransaction, actuanMerchanTransactionDTO);
    }

    @Then("the merchant report is broadcastet")
    public void theMerchantReportIsBroadcastet() {
        assertEquals(actualRoute, "report.generated." + reportRequestId);
    }

}
