package listeners;

import DTO.PaymentTransactionDTO;
import com.google.gson.Gson;
import core.AsyncService;
import providers.ExchangeProvider;
import services.ReportingService;

/**
 *  Listener for listening on completed refunds
 * @author Mathias
 */
public class RefundCompletedListener extends AsyncService<PaymentTransactionDTO> {

    private final ReportingService reportingService;


    public RefundCompletedListener(ReportingService reportingService) {
        super(ExchangeProvider.getPaymentExchange(), "transaction.refund.handled.*");
        this.reportingService = reportingService;
    }

    @Override
    protected PaymentTransactionDTO handleMessage(String message) {
        return new Gson().fromJson(message, PaymentTransactionDTO.class);
    }

    @Override
    public void receive(PaymentTransactionDTO paymentResponseDTO) {
        reportingService.reportTransaction(paymentResponseDTO, true);
    }
}
