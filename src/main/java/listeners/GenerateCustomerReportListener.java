package listeners;

import DTO.ReportGenerationRequestDTO;
import com.google.gson.Gson;
import converters.DatetimeConverter;
import providers.ExchangeProvider;
import core.AsyncService;
import services.GenerateReportService;

/**
 * Listener for events that requests a report to be generated for a customer
 * @author Mathias
 */
public class GenerateCustomerReportListener extends AsyncService<ReportGenerationRequestDTO> {
    private final GenerateReportService generateReportService;

    public GenerateCustomerReportListener(GenerateReportService generateReportService) {
        super(ExchangeProvider.getReportingExchange(), "reporting.customer.request.*");
        this.generateReportService = generateReportService;
    }

    @Override
    protected ReportGenerationRequestDTO handleMessage(String message) {
        return new Gson().fromJson(message, ReportGenerationRequestDTO.class);
    }

    @Override
    public void receive(ReportGenerationRequestDTO request) {
        generateReportService.requestReport(request.userId, DatetimeConverter.ConvertStringToDate(request.startDate),
                DatetimeConverter.ConvertStringToDate(request.endDate), request.reportGenerationRequestId);
    }
}
