package DTO;

import java.time.LocalDate;

/**
 *  * Represents a merchant transaction which is the information stored in a merchant report
 * @author Mathias
 */
public class MerchantTransactionDTO extends BaseTransactionDTO{

    public String merchantId;

    public MerchantTransactionDTO(String merchantId, String token, float amount, LocalDate date) {
        super(token, amount, date);
        this.merchantId = merchantId;
    }
}