package repositories.customer;

import entities.CustomerTransaction;

import java.time.LocalDate;
import java.util.List;

/**
 * The repository for {@link CustomerTransaction}
 * @author Mathias
 */
public interface CustomerTransactionRepository  {
    CustomerTransaction add(CustomerTransaction transaction);
    List<CustomerTransaction> getCustomerTransactions(String customerId, LocalDate from, LocalDate to);
}
