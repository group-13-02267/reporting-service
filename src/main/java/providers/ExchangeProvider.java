package providers;

/**
 * Provider class for getting the name of an exchange
 */
public class ExchangeProvider {
    public static String getReportingExchange(){
        return "reporting";
    }
    public static String getPaymentExchange(){
        return "payment";
    }
}
