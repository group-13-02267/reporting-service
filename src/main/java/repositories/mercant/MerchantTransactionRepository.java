package repositories.mercant;

import entities.MerchantTransaction;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Mathias
 */
public interface MerchantTransactionRepository {
    MerchantTransaction add(MerchantTransaction transaction);
    List<MerchantTransaction> getMerchantTransactions(String merchantId, LocalDate from, LocalDate to);
}
