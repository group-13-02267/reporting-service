package DTO;

/**
 * Represents a request for generating a report
 * @author Mathias
 */
public class ReportGenerationRequestDTO {

    public String reportGenerationRequestId;
    public String userId;
    public String startDate;
    public String endDate;

    public ReportGenerationRequestDTO(String reportGenerationRequestId, String userId, String startDate, String endDate) {
        this.reportGenerationRequestId = reportGenerationRequestId;
        this.userId = userId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public ReportGenerationRequestDTO() {
    }
}
