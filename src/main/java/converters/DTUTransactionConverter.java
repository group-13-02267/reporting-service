package converters;

import DTO.PaymentTransaction;
import entities.CustomerTransaction;
import entities.DTUCustomerTransaction;
import entities.DTUMerchantTransaction;
import entities.MerchantTransaction;

/**
 * Responsiple for converting from a {@link PaymentTransaction} to a merchant and customer transaction
 * @author Mathias
 */
public class DTUTransactionConverter implements TransactionConverter {

    /**
     * @param paymentTransaction instance of {@link PaymentTransaction}
     * @param isRefund {@link Boolean} which indicates if the paymenTransaction is a refund
     * @return a {@link CustomerTransaction} containing the relevant information of the paymentTransaction
     */
    @Override
    public CustomerTransaction toCustomerTransaction(PaymentTransaction paymentTransaction, boolean isRefund) {
        String customerId = null;
        if (isRefund){
            customerId = paymentTransaction.getReceiver();
        }
        else{
            customerId = paymentTransaction.getSender();
        }
        return new DTUCustomerTransaction(paymentTransaction.getToken(), paymentTransaction.getAmount(),
                                          customerId, paymentTransaction.getDate());
    }

    /**
     * @param paymentTransaction instance of {@link PaymentTransaction}
     * @param isRefund {@link Boolean} which indicates if the paymenTransaction is a refund
     * @return a {@link MerchantTransaction} containing the relevant information of the paymentTransaction
     */
    @Override
    public MerchantTransaction toMerchantTransaction(PaymentTransaction paymentTransaction, boolean isRefund) {
        String merchantId = null;
        if (isRefund){
            merchantId = paymentTransaction.getSender();
        }
        else{
            merchantId = paymentTransaction.getReceiver();
        }
        return new DTUMerchantTransaction(paymentTransaction.getToken(), paymentTransaction.getAmount(),
                merchantId, paymentTransaction.getDate());
    }
}
