package repositories.customer;

import entities.CustomerTransaction;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * In-memory database implementation of {@link CustomerTransactionRepository}
 * @author Mathias
 */
public class InMemoryCustomerTransactionRepository  implements CustomerTransactionRepository{
    private static InMemoryCustomerTransactionRepository instance;

    public static InMemoryCustomerTransactionRepository getInstance() {
        if (instance == null) {
            instance = new InMemoryCustomerTransactionRepository();
        }
        return instance;
    }

    protected InMemoryCustomerTransactionRepository() {
    }

    List<CustomerTransaction> customerTransactions = new ArrayList<>();

    /***
     * Deletes all stored data.
     */
    public static void clean() {
        InMemoryCustomerTransactionRepository.getInstance().customerTransactions = new ArrayList<>();
    }


    @Override
    public CustomerTransaction add(CustomerTransaction transaction) {
        customerTransactions.add(transaction);
        return transaction;
    }

    @Override
    public List<CustomerTransaction> getCustomerTransactions(String customerId, LocalDate from, LocalDate to) {
        return customerTransactions.stream()
                .filter((p) -> p.getCustomerId().equals(customerId) && p.getDate().isBefore(to.plusDays(1)) &&
                        p.getDate().isAfter(from.minusDays(1))).collect(Collectors.toList());
    }
}
