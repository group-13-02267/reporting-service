package services;

import messaging.ReportingMessagingService;

import java.time.LocalDate;

/**
 * Implementation of {@link GenerateReportService} for generating reports for a merchant
 * @author Mathias
 */
public class MerchantGenerateReportService implements GenerateReportService {
    private final ReportingService reportingService;
    private final ReportingMessagingService reportingMessagingService;

    public MerchantGenerateReportService(ReportingService reportingService, ReportingMessagingService reportingMessagingService) {
        this.reportingService = reportingService;
        this.reportingMessagingService = reportingMessagingService;
    }

    @Override
    public void requestReport(String userId, LocalDate startDate, LocalDate endDate, String reportGenerationRequestId) {
        String report = reportingService.generateMerchantReport(userId, startDate, endDate);
        reportingMessagingService.SendReport(report, reportGenerationRequestId);
    }
}
